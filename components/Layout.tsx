import Head from 'next/head';
import NavBar from './Navbar';
import Footer from './Footer';

interface LayoutProps {
  children: JSX.Element;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <>
      <Head>
        <title>Real Estate</title>
      </Head>
      <div className="max-w-[1480px] m-auto">
        <header>
          <NavBar />
        </header>
        <main>{children}</main>
        <Footer />
      </div>
    </>
  );
};

export default Layout;
