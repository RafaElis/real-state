import { FC } from 'react';
import Image from 'next/image';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

interface ImageScrollbarProps {
  data: {
    id?: number;
    externalID: string;
    title: string;
    url: string;
    orderIndex: number;
    nimaScore: number;
  }[];
}

const settings = {
  dots: true,
  infinite: true,
  arrows: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  adaptiveHeight: true
};

const ImageScrollbar: FC<ImageScrollbarProps> = ({ data }) => (
  <Slider {...settings}>
    {data.map(image => (
      <div className="p-1" key={image.id} itemID={`${image.id}`}>
        <Image
          alt="property"
          placeholder="blur"
          src={image.url}
          blurDataURL={image.url}
          width={1000}
          height={500}
          // layout='fill'
          sizes="(max-width: 500px), 100px, (max-width): 1023px 400px, 1000px"
        />
      </div>
    ))}
  </Slider>
);

export default ImageScrollbar;
