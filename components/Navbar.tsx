import { FC, forwardRef, Fragment } from 'react';
import { Menu, Transition } from '@headlessui/react';
import { FcMenu, FcHome, FcAbout } from 'react-icons/fc';
import { BsSearch } from 'react-icons/bs';
import { FiKey } from 'react-icons/fi';
import Link from 'next/link';

const menuItens = [
  { icon: <FcHome />, text: 'Inicio', route: '/' },
  { icon: <BsSearch />, text: 'Procurar', route: '/search' },
  { icon: <FcAbout />, text: 'Comprar', route: '/search?purpose=for-sale' },
  { icon: <FiKey />, text: 'Alugar', route: '/search?purpose=for-rent' }
];

interface ItemProps {
  href: string;
  children: React.ReactNode;
}

const Item = forwardRef<HTMLAnchorElement, ItemProps>((props, ref) => {
  const { href, children, ...rest } = props;
  return (
    <Link href={href}>
      <a ref={ref} {...rest}>
        {children}
      </a>
    </Link>
  );
});

Item.displayName = 'Item';

const Navbar: FC = () => {
  return (
    <div className="flex border-b border-gray-100 p-2 justify-between">
      <div className="text-3xl text-blue-400 font-bold pl-2">
        <Link href="/">Realtor</Link>
      </div>
      <div>
        <Menu as="div">
          <div>
            <Menu.Button className="flex justify-center items-center rounded-md w-10 h-10 border border-[#e8e4df] hover:bg-gray-100">
              <FcMenu />
            </Menu.Button>
          </div>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Menu.Items className="absolute mt-2 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
              {menuItens.map(({ icon, text, route }) => (
                <Menu.Item key={text}>
                  {() => (
                    <Item href={route}>
                      <div className="flex items-center px-3 pr-6 py-2 hover:bg-gray-200 cursor-pointer">
                        <span className="mr-3">{icon}</span>
                        {text}
                      </div>
                    </Item>
                  )}
                </Menu.Item>
              ))}
            </Menu.Items>
          </Transition>
        </Menu>
      </div>
    </div>
  );
};

export default Navbar;
