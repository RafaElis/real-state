import Link from 'next/link';
import Image from 'next/image';
import { FaBed, FaBath } from 'react-icons/fa';
import { BsGridFill } from 'react-icons/bs';
import { GoVerified } from 'react-icons/go';

import defaultImage from 'assets/images/house.jpg';
import { FC } from 'react';

interface PropertyProps {
  property: IPropertyInfo;
}

const formatPrice = (price: number): string =>
  price.toLocaleString('pt-BR', { currency: 'BRL', style: 'currency' });

const formatArea = (area: number): string =>
  area.toLocaleString('pt-BR', { maximumFractionDigits: 0 });

const Property: FC<PropertyProps> = ({
  property: {
    coverPhoto,
    price,
    rentFrequency,
    rooms,
    title,
    baths,
    area,
    agency,
    isVerified,
    externalID
  }
}) => (
  <Link href={`/property/${externalID}`} passHref>
    <div className="flex flex-wrap justify-start w-[420px] p-4 cursor-pointer border border-[#e8e4df] rounded-md">
      <div className="rounded-2xl overflow-hidden">
        <Image
          src={coverPhoto ? coverPhoto.url : defaultImage}
          width={400}
          height={260}
          alt={title}
        />
      </div>
      <div className="w-full">
        <div className="flex pt-2 justify-between items-center">
          <div className="flex items-center">
            <div className="pr-3 text-green-400">
              {isVerified && <GoVerified />}
            </div>
            <div>
              <p className="text-lg font-bold">
                {title.length > 30 ? `${title.substring(0, 30)}...` : title}
              </p>
            </div>
          </div>
          <div className="rounded-full overflow-hidden w-8 h-8">
            <Image width={32} height={32} alt="avatar" src={agency.logo.url} />
          </div>
        </div>
        <div className="flex justify-between items-center mt-2">
          <div className="flex items-center">
            <FaBed /> <p className="ml-1">{rooms} Quarto(s)</p>
          </div>
          <div className="flex items-center">
            <FaBath /> <p className="ml-1">{baths} Banheiro(s)</p>
          </div>
          <div className="flex items-center">
            <BsGridFill /> <p className="ml-1">{formatArea(area)} m²</p>
          </div>
        </div>
        <div className="flex justify-between items-center mt-2">
          <p className="text-lg font-bold">
            {formatPrice(price)} {rentFrequency && `/${rentFrequency}`}
          </p>
          <button className="bg-transparent border border-[#e8e4df] rounded-lg p-3 hover:bg-slate-200">
            Detalhes
          </button>
        </div>
      </div>
    </div>
  </Link>
);

export default Property;
