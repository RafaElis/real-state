import { FC } from 'react';

const Footer: FC = () => {
  return (
    <footer className="flex justify-center items-center border-t border-t-gray-500 h-16">
      <div className="text-center text-gray-500 p-5">2020 © Real Estate</div>
    </footer>
  );
};

export default Footer;
