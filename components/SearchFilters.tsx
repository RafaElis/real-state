import { FC } from 'react';
import { useRouter } from 'next/router';

import type { FilterValuesProps } from 'utils/filterData';
import { filterData, getFilterValues } from 'utils/filterData';

const SearchFilters: FC = () => {
  const router = useRouter();

  const searchProperties = (filterValues: FilterValuesProps) => {
    const { query } = router;
    const path = router.pathname;
    const values = getFilterValues(filterValues);
    values.forEach(item => {
      query[item.name] = item.value;
    });
    router.push({ pathname: path, query });
  };

  return (
    <div className="flex flex-wrap justify-center p-4 bg-gray-100 gap-4 ">
      {filterData.map(filter => (
        <div key={filter.queryName}>
          <select
            placeholder={filter.placeholder}
            className="
              w-fit
              p-2
              bg-transparent
              border-gray-200
              hover:border-gray-400
              border
              rounded-md
            "
            onChange={e =>
              searchProperties({ [filter.queryName]: e.target.value })
            }
          >
            {filter.items.map(item => (
              <option value={item.value} key={item.value}>
                {item.name}
              </option>
            ))}
          </select>
        </div>
      ))}
    </div>
  );
};

export default SearchFilters;
