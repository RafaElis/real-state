import { FC } from 'react';
import type { NextPage } from 'next';
import Link from 'next/link';
import Image from 'next/image';
import { get } from 'utils/http';
import Property from 'components/Property';

interface BannerProps {
  purpose: string;
  imageUrl: string;
  title1: string;
  title2: string;
  linkPath: string;
  buttonText: string;
  desc1: string;
  desc2: string;
}

const Banner: FC<BannerProps> = ({
  purpose,
  imageUrl,
  title1,
  title2,
  linkPath,
  buttonText,
  desc1,
  desc2
}) => (
  <div className="flex flex-wrap justify-center items-center m-10">
    <Image src={imageUrl} width={500} height={300} alt="banner" />
    <div className="p-5">
      <p className="font-medium text-gray-500 text-sm">{purpose}</p>
      <p className="font-bold text-3xl">
        {title1}
        <br /> {title2}
      </p>
      <p className="text-gray-700 text-lg font-medium py-3">
        {desc1}
        <br /> {desc2}
      </p>
      <button className="text-xl bg-blue-300 hover:bg-blue-500 text-white p-3 rounded-lg">
        <Link href={linkPath}>{buttonText}</Link>
      </button>
    </div>
  </div>
);

interface IHome {
  propertyForSale: IPropertyInfo[];
  propertyForRent: IPropertyInfo[];
}

const Home: NextPage<IHome> = ({ propertyForSale, propertyForRent }) => {
  return (
    <div className="mb-4">
      <Banner
        purpose="COMPRAR CASA"
        title1="Procure e compre"
        title2="a casas dos seus sonhos"
        desc1="Explorar Apartamentos, Condominios, Casas"
        desc2="e muito mais"
        linkPath="/search?purpose=for-sale"
        buttonText="Buscar Casas"
        imageUrl="https://bayut-production.s3.eu-central-1.amazonaws.com/image/145426814/33973352624c48628e41f2ec460faba4"
      />
      <div className="flex flex-wrap justify-center gap-4">
        {propertyForSale.map(property => (
          <Property property={property} key={property.id} />
        ))}
      </div>
      <Banner
        purpose="ALUGAR CASA"
        title1="Comprar casa para"
        title2="todos"
        desc1="Explorar Apartamentos, Condominios, Casas"
        desc2="e muito mais"
        linkPath="/search?purpose=for-rent"
        buttonText="Buscar Casas"
        imageUrl="https://bayut-production.s3.eu-central-1.amazonaws.com/image/145426814/33973352624c48628e41f2ec460faba4"
      />
      <div className="flex flex-wrap justify-center gap-4">
        {propertyForRent.map(property => (
          <Property property={property} key={property.id} />
        ))}
      </div>
    </div>
  );
};

export async function getStaticProps() {
  const propertyForSale = await get<IProperty>('properties/list', {
    locationExternalIDs: '5002,6020',
    purpose: 'for-sale',
    hitsPerPage: '25'
  });
  const propertyForRent = await get<IProperty>('properties/list', {
    locationExternalIDs: '5002,6020',
    purpose: 'for-rent',
    hitsPerPage: '25'
  });

  return {
    props: {
      propertyForSale: propertyForSale.hits,
      propertyForRent: propertyForRent.hits
    }
  };
}

export default Home;
