import type { NextPage } from 'next';
import { get } from 'utils/http';
import ImageScrollbar from 'components/ImageScrollbar';
import { GoVerified } from 'react-icons/go';
import { FaBath, FaBed } from 'react-icons/fa';
import { BsGridFill } from 'react-icons/bs';
import millify from 'millify';
import Image from 'next/image';

interface PropertyDetailsProps {
  propertyDetails: IPropertyDetail;
}

const PropertyDetails: NextPage<PropertyDetailsProps> = ({
  propertyDetails: {
    title,
    description,
    price,
    agency,
    amenities,
    area,
    baths,
    photos,
    purpose,
    rooms,
    type,
    furnishingStatus,
    rentFrequency,
    isVerified
  }
}) => {
  return (
    <div className="m-auto p-4">
      {photos && <ImageScrollbar data={photos} />}
      <div className="w-full p-6">
        <div className="flex items-center justify-between pt-2">
          <div className="flex items-center">
            <div className="pr-3 text-green-400">
              {isVerified && <GoVerified />}
            </div>
            <p className="font-bold text-lg">
              {'AED '}
              {millify(price)} {rentFrequency && `/${rentFrequency}`}
            </p>
          </div>
          <div className="rounded-full overflow-hidden w-8 h-8">
            <Image width={32} height={32} alt="avatar" src={agency.logo.url} />
          </div>
        </div>
        <div className="flex items-center p-1 justify-between text-blue-400 w-[15.625rem]">
          {rooms} <FaBed />
          {' | '}
          {baths} <FaBath />
          {' | '}
          {millify(area)} <BsGridFill />
        </div>
        <div className="mt-2">
          <p className="text-lg font-bold mb-2">{title}</p>
          <p className="text-gray-600 leading-8">{description}</p>
        </div>
        <div className="flex gap-4 flex-wrap uppercase justify-between">
          <div className="flex sm:grow grow-0 justify-between w-[25rem] border-b-[10px] border-gray-200 p-3">
            <p>Tipo</p>
            <p className="font-bold">{type}</p>
          </div>
          <div className="flex sm:grow grow-0 justify-between w-[25rem] border-b-[10px] border-gray-200 p-3">
            <p>Propósito</p>
            <p className="font-bold">{purpose}</p>
          </div>
          {furnishingStatus && (
            <div className="flex sm:grow grow-0 justify-between w-[25rem] border-b-[10px] border-gray-200 p-3">
              <p>Mobiliado</p>
              <p className="font-bold">{furnishingStatus}</p>
            </div>
          )}
        </div>
        <div>
          {amenities.length > 0 && (
            <p className="text-2xl font-black mt-5">Amenities</p>
          )}
          <div className="flex flex-wrap">
            {amenities.map(item =>
              item.amenities.map(amenity => (
                <p
                  key={amenity.text}
                  className="font-bold text-blue-400 bg-gray-200 m-1 p-2 rounded-3xl"
                >
                  {amenity.text}
                </p>
              ))
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

interface ServerSideProps {
  params: {
    id: string | number;
  };
}

export const getServerSideProps = async ({
  params: { id }
}: ServerSideProps) => {
  const data = await get<IPropertyDetail>('properties/detail', {
    externalID: id
  });
  return {
    props: {
      propertyDetails: data || {}
    }
  };
};

export default PropertyDetails;
