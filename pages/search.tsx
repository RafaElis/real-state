import type { NextPage } from 'next';
import { useState } from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import type { NextRouter } from 'next/router';
import { BsFilter } from 'react-icons/bs';
import SearchFilters from 'components/SearchFilters';
import Property from 'components/Property';
import noresult from 'assets/images/noresult.svg';
import { get } from 'utils/http';

interface RouterProps extends NextRouter {
  query: {
    purpose: string;
    // purpose: 'for-sale' | 'for-rent';
  };
}

interface SearchProps {
  properties: IPropertyInfo[];
}

const Search: NextPage<SearchProps> = ({ properties }) => {
  const [searchFilter, setSearchFilter] = useState(false);
  const router = useRouter() as RouterProps;
  const handleSearchFilter = () => setSearchFilter(prev => !prev);

  return (
    <div>
      <div
        className="
          flex
          justify-center
          items-center
          p-2
          bg-gray-100
          border-b
          border-gray-200
          text-lg
          font-black
          cursor-pointer
        "
        onClick={handleSearchFilter}
      >
        <p>Search Property by Filters</p>
        <BsFilter className="pl-2 w-7" />
      </div>
      {searchFilter && <SearchFilters />}
      <p className="text-2xl p-4 font-bold">
        Properties {router.query.purpose}
      </p>
      <div className="flex flex-wrap justify-center gap-4">
        {properties.map(property => (
          <Property key={property.id} property={property} />
        ))}
      </div>
      {properties.length === 0 && (
        <div className="flex flex-col justify-center items-center mt-5 mb-5">
          <Image alt="sem resultado" src={noresult} />
          <p className="text-2xl mt-3">Nenhum propiedade foi encontrada</p>
        </div>
      )}
    </div>
  );
};

interface ServerSideProps {
  query: {
    purpose?: string;
    rentFrequency?: string;
    minPrice?: string;
    maxPrice?: string;
    roomsMin?: string;
    bathsMin?: string;
    sort?: string;
    areaMax?: string;
    locationExternalIDs?: string;
    categoryExternalID?: string;
  };
}

export async function getServerSideProps({ query }: ServerSideProps) {
  const purpose = query.purpose || 'for-rent';
  const rentFrequency = query.rentFrequency || 'yearly';
  const minPrice = query.minPrice || '0';
  const maxPrice = query.maxPrice || '1000000';
  const roomsMin = query.roomsMin || '0';
  const bathsMin = query.bathsMin || '0';
  const sort = query.sort || 'price-desc';
  const areaMax = query.areaMax || '35000';
  const locationExternalIDs = query.locationExternalIDs || '5002';
  const categoryExternalID = query.categoryExternalID || '4';

  const properties = await get<IProperty>('properties/list', {
    locationExternalIDs,
    purpose,
    categoryExternalID,
    bathsMin,
    rentFrequency,
    priceMin: minPrice,
    priceMax: maxPrice,
    roomsMin,
    sort,
    areaMax
  });

  return {
    props: {
      properties: properties.hits
    }
  };
}

export default Search;
