interface IPropertyDetail {
  id?: number;
  price: number;
  rentFrequency?: string;
  rooms: number;
  title: string;
  baths: number;
  area: number;
  agency: {
    id?: number;
    objectID: number;
    name: string;
    externalID:string;
    product: string;
    productScore:number;
    licenses: {
      number: string;
      authority:string;
    }[];
    logo: {
      id?: number;
      url: string;
    },
    slug: string;
    tier: number;
  };
  isVerified: boolean;
  description: string;
  type: string;
  purpose: string;
  furnishingStatus?: string;
  amenities: {
    text: string;
    amenities: {
      text: string;
    }[]
  }[];
  photos: {
    id?: number;
    externalID: string;
    title: string;
    url: string;
    orderIndex: number;
    nimaScore: number;
  }[];
}
