interface IPropertyInfo {
  id: number;
  coverPhoto: {
    url: string;
  };
  price: number;
  rentFrequency: string;
  rooms: number;
  title: string;
  baths: number;
  area: number;
  isVerified: boolean;
  externalID: string;
  agency: {
    id: string;
    name: string;
    logo: {
      id: number;
      url: string;
    };
  };
}

interface IProperty {
  hits: IPropertyInfo[];
  nbHits: number;
  page: number;
  nbPages: number;
  hitsPerPage: number;
  exhaustiveNbHits: boolean;
  query: string;
  params: string;
  processingTimeMS: number;
}
