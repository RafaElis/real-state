import axios from 'axios';

const baseUrl = 'https://bayut.p.rapidapi.com';

export const http = axios.create({
  baseURL: baseUrl,
  headers: {
    'x-rapidapi-key': '291218bb69msh931ed328bc365fap192699jsn710b60d8089c',
    'x-rapidapi-host': 'bayut.p.rapidapi.com'
  }
});

export const get = async <T>(url: string, params?: object): Promise<T> => {
  const response = await http.get(`${baseUrl}/${url}`, { params: params });
  return response.data;
};

export const post = async (url: string, data?: unknown, params?: unknown) => {
  const response = await http.post(`${baseUrl}/${url}`, data, { params });
  return response.data;
};

export const put = async (url: string, data?: unknown, params?: unknown) => {
  const response = await http.put(`${baseUrl}/${url}`, data, { params });
  return response.data;
};

export const deleteRequest = async (url: string, params?: unknown) => {
  const response = await http.delete(`${baseUrl}/${url}`, { params });
  return response.data;
};